package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import party.zjut.jw.model.Album;
import party.zjut.jw.model.Status;
import party.zjut.jw.service.AlbumService;
import party.zjut.jw.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/v1/albums")
public class AlbumController {

    @Autowired
    UserService userService;
    @Autowired
    AlbumService albumService;

    /**
     * Get all of my albums
     * GET /api/v1/albums
     *
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    Status getAllAlbums(@RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            List<Album> albumList = albumService.getAlbumList();
            if (albumList != null) {
                return new Status(200, albumList);
            } else {
                return new Status(500, "Server Error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Create a new album
     * POST /api/v1/albums
     *
     * @param album String
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody
    Status addAlbum(@ModelAttribute Album album, @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            album.setUserId(userService.getUserByAccount(account).getId());
            if (albumService.addAlbum(album)) {
                return new Status(201, "Upload success");
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Get a album
     * GET /api/v1/albums/:album_id
     *
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Status getAlbum(@PathVariable("id") int id, @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            Album album = albumService.getAlbumById(id);
            if (album != null) {
                return new Status(200, album);
            } else {
                return new Status(404, "No such album");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Update album info
     * PUT /api/v1/albums/:album_id
     *
     * @param id int
     * @param album Album
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    Status getAlbum(@PathVariable("id") int id, @ModelAttribute Album album,
                    @RequestParam String account, String password) {
        if (!userService.authUser(account, password)) {
            return new Status(401, "Not authorized");
        }
        if (!checkOwner(account, id)) {
            return new Status(403, "Forbidden");
        }
        if (albumService.modAlbum(album)) {
            return new Status(200, "OK");
        } else {
            return new Status(500, "Error");
        }
    }

    /**
     * Delete a album
     * DELETE /api/v1/albums/:album_id
     *
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delAlbum(@PathVariable("id") int id, @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            if (checkOwner(account, id)) {
                if (albumService.delAlbum(id)) {
                    return new Status(204, "Album deleted successfully!");
                } else {
                    return new Status(500, "Server Error");
                }
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    private boolean checkOwner(String account, int id) {
        return albumService.checkOwner(userService.getUserByAccount(account).getId(), id);
    }

}
