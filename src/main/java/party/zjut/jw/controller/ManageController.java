package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import party.zjut.jw.model.*;
import party.zjut.jw.service.CommentService;
import party.zjut.jw.service.PostService;
import party.zjut.jw.service.TagService;
import party.zjut.jw.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/v1/manage")
public class ManageController {

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    CommentService commentService;
    @Autowired
    TagService tagService;

    @RequestMapping(value = "/users/{user_id}", method = RequestMethod.GET)
    public @ResponseBody
    Status getUserInfo(@PathVariable(value = "user_id") int userId,
                       @RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            return new Status(200, userService.getUserById(userId));
        } else {
            return status;
        }
    }

    @RequestMapping(value = "/users/{user_id}", method = RequestMethod.POST)
    public @ResponseBody
    Status updateUserInfo(@PathVariable(value = "user_id") int userId,
                       @RequestBody Payload payload,
                       @RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            User user = userService.getUserById(userId);
            if (user == null) {
                return new Status(404, "No such user");
            } else {
                user.setManager(payload.isManager());
                userService.modUser(user);
                return new Status(201, user);
            }
        } else {
            return status;
        }
    }

    /**
     * Change post status
     */
    @RequestMapping(value = "/posts/{post_id}", method = RequestMethod.POST)
    public @ResponseBody
    Status updatePostStatus(@PathVariable(value = "post_id") int postId,
                            @RequestBody Payload payload,
                            @RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            Post post = postService.getPostById(postId);
            if (post == null) {
                return new Status(404, "No such post");
            } else {
                post.setPublicity(payload.getPublicity());
                if (postService.modPost(post)) {
                    return new Status(201, post);
                } else {
                    return new Status(500, "Server error");
                }
            }
        } else {
            return status;
        }
    }

    /**
     * Delete a comment
     */
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status deleteComment(@PathVariable("id") int id,
                         @RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            Comment comment = commentService.getCommentById(id);
            if (comment == null) {
                return new Status(404, "No such comment");
            }
            if (commentService.delComment(id)) {
                return new Status(204, "Deleted");
            } else {
                return new Status(500, "Server error");
            }
        } else {
            return status;
        }
    }

    /**
     * Get all tags
     */
    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public @ResponseBody
    Status getAllTags(@RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            List<Tag> tags = tagService.getAllTags();
            if (tags != null) {
                return new Status(200, tags);
            } else {
                return new Status(500, "Server error");
            }
        } else {
            return status;
        }
    }

    /**
     * Delete a tag
     */
    @RequestMapping(value = "/tags/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status deleteTag(@PathVariable("id") int id,
                     @RequestParam String account, String password) {
        Status status = authAdmin(account, password);
        if (status == null) {
            Tag tag = tagService.getTagById(id);
            if (tag == null) {
                return new Status(404, "No such tag");
            }
            if (tagService.delTag(tag)) {
                return new Status(204, "Deleted");
            } else {
                return new Status(500, "Server error");
            }
        } else {
            return status;
        }
    }

    private Status authAdmin(String account, String password) {
        if (userService.authUser(account, password)) {
            User admin = userService.getUserByAccount(account);
            if (admin.isManager()) {
                return null;
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Authorize failed");
        }
    }

}
