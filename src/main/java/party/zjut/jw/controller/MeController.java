package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import party.zjut.jw.model.Bookmark;
import party.zjut.jw.model.Payload;
import party.zjut.jw.model.Status;
import party.zjut.jw.model.User;
import party.zjut.jw.service.BookmarkService;
import party.zjut.jw.service.PostService;
import party.zjut.jw.service.UserService;


@Controller
@RequestMapping("/v1/me")
public class MeController {

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    BookmarkService bookmarkService;

    // Profile

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    Status getMyProfile(@RequestParam String account, String password) {
        boolean result = userService.authUser(account, password);
        if (result) {
            User user = userService.getUserByAccount(account);
            return new Status(200, user);
        } else {
            return new Status(401, "Not authorized");
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)    // PATCH
    public @ResponseBody
    Status modMyProfile(@ModelAttribute User user) {
        if (userService.authUser(user.getAccount(), user.getPassword())) {
            if (userService.modUser(user)) {
                return new Status(202, "Accepted");
            } else {
                return new Status(500, "Unknown error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    // Bookmarks

    @RequestMapping(value = "/bookmarks", method = RequestMethod.GET)
    public @ResponseBody
    Status getBookmarks(@RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            int userId = userService.getUserByAccount(account).getId();
            return new Status(200, bookmarkService.getBookmarksByUserId(userId));
        } else {
            return new Status(401, "Not authorized");
        }
    }

    @RequestMapping(value = "/bookmarks", method = RequestMethod.POST)
    public @ResponseBody
    Status addBookmark(@RequestBody Payload bk,
                       @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            Integer postId = bk.getPostId();
            Integer rating = bk.getRating();
            if (bookmarkService.addBookmark(account, postId, rating)) {
                return new Status(201, "Bookmark done");
            } else {
                return new Status(500, "Unknown error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    @RequestMapping(value = "/bookmarks/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delBookmark(@PathVariable("id") int id,
                       @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            User user = userService.getUserByAccount(account);
            Bookmark bk = bookmarkService.getBookmarkById(id);
            if (bk.getUserId() == user.getId()) {
                if (bookmarkService.delBookmark(id)) {
                    return new Status(204, "Bookmark deleted");
                } else {
                    return new Status(500, "Unknown error");
                }
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    // Posts

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public @ResponseBody
    Status getMyPosts(@RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            int userId = userService.getUserByAccount(account).getId();
            return new Status(200, postService.getPostsByUserId(userId));
        } else {
            return new Status(401, "Not authorized");
        }
    }

}
