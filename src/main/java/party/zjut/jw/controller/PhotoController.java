package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import party.zjut.jw.model.*;
import party.zjut.jw.service.PhotoService;
import party.zjut.jw.service.TagService;
import party.zjut.jw.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/v1/photos")
public class PhotoController {

    @Autowired
    UserService userService;
    @Autowired
    PhotoService photoService;
    @Autowired
    TagService tagService;

    /**
     * Get all of my photos
     * GET /api/v1/photos
     *
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    Status getAllPhotos(@RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            User user = userService.getUserByAccount(account);
            List<Photo> photoList = photoService.getPhotosByUserId(user.getId());
            if (photoList != null) {
                return new Status(200, photoList);
            } else {
                return new Status(500, "Server Error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Create a new photo
     * POST /api/v1/photos
     *
     * @param photo String
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody
    Status addPhoto(@RequestBody Photo photo,
                    @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            photo.setUserId(userService.getUserByAccount(account).getId());
            if (photoService.addPhoto(photo)) {
                return new Status(201, photo);
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Get a photo
     * GET /api/v1/photos/:photo_id
     *
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Status getPhoto(@PathVariable("id") int id, @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            Photo photo = photoService.getPhotoById(id);
            if (photo != null) {
                return new Status(200, photo);
            } else {
                return new Status(404, "No such photo");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Update photo info
     * PUT /api/v1/photos/:photo_id
     *
     * @param id int
     * @param photo Photo
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    Status updatePhoto(@PathVariable("id") int id,
                    @RequestBody Photo photo,
                    @RequestParam String account, String password) {
        if (!userService.authUser(account, password)) {
            return new Status(401, "Not authorized");
        }
        if (!checkOwner(account, id)) {
            return new Status(403, "Forbidden");
        }
        if (photoService.modPhoto(photo)) {
            return new Status(202, photo);
        } else {
            return new Status(500, "Error");
        }
    }

    /**
     * Delete a photo
     * DELETE /api/v1/photos/:photo_id
     *
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delPhoto(@PathVariable("id") int id, @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            if (checkOwner(account, id)) {
                if (photoService.delPhoto(id)) {
                    return new Status(204, "Photo deleted successfully!");
                } else {
                    return new Status(500, "Server Error");
                }
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    private boolean checkOwner(String account, int id) {
        return photoService.checkOwner(userService.getUserByAccount(account).getId(), id);
    }


    /**
     * Get tags of photo
     * GET /api/v1/photos/:photo_id/tags
     *
     * @param photo_id int
     * @return Status
     */
    @RequestMapping(value = "/{photo_id}/tags", method = RequestMethod.GET)
    public @ResponseBody
    Status getPhotoTags(@PathVariable("photo_id") int photo_id) {
        Photo photo = photoService.getPhotoById(photo_id);
        return new Status(200, photo.getTags());
    }

    @RequestMapping(value = "/{photo_id}/tags", method = RequestMethod.POST)
    public @ResponseBody
    Status addPhotoTag(@PathVariable("photo_id") int photo_id,
                       @RequestBody Payload payload,
                       @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            Photo photo = photoService.getPhotoById(photo_id);
            Tag tag = new Tag();
            tag.setName(payload.getName());
            if (tagService.addPhotoTag(photo, tag)) {
                return new Status(201, "OK");
            } else {
                return new Status(500, "Server error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    @RequestMapping(value = "/{photo_id}/tags/{tag_name}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delPhotoTag(@PathVariable("photo_id") int photo_id,
                       @PathVariable("tag_name") String tag_name,
                       @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            Tag itag = tagService.getTagByName(tag_name);
            if (itag == null) {
                return new Status(404, "No such tag");
            } else {
                Photo photo = photoService.getPhotoById(photo_id);
                if (tagService.delPhotoTag(photo, itag)) {
                    return new Status(204, "Tag deleted");
                } else {
                    return new Status(500, "Server error");
                }

            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody
    Status uploadImage(@RequestParam("file") MultipartFile le,
                       @RequestParam String account, String password) {
        if (le.isEmpty()) {
            return new Status(400, "Empty file");
        }
        if (userService.authUser(account, password)) {
            String filename = le.getOriginalFilename();
            if (photoService.savePhoto(le, filename)) {
                return new Status(201, filename + ": " + le.getSize() + "Bytes ");
            } else {
                return new Status(500, "Server Error " + filename);
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }
}
