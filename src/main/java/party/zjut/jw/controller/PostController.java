package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import party.zjut.jw.model.Comment;
import party.zjut.jw.model.Payload;
import party.zjut.jw.model.Post;
import party.zjut.jw.model.Status;
import party.zjut.jw.service.CommentService;
import party.zjut.jw.service.PhotoService;
import party.zjut.jw.service.PostService;
import party.zjut.jw.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/v1/posts")
public class PostController {

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    PhotoService photoService;
    @Autowired
    CommentService commentService;

    /**
     * Get all posts
     * GET /api/v1/posts
     *
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    Status getAllPosts() {
        List<Post> postList = postService.getPostList();
        if (postList != null) {
            return new Status(200, postList);
        } else {
            return new Status(500, "Server Error");
        }
    }

    /**
     * Create a new post
     * POST /api/v1/posts
     *
     * @param payload String
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseBody
    Status addPost(@RequestBody Payload payload,
                   @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            if (postService.addPost(payload)) {
                return new Status(201, "Upload success");
            } else {
                return new Status(500, "Server Error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Get a post
     * GET /api/v1/posts/:post_id
     *
     * @param id int
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Status getPost(@PathVariable("id") int id) {
        Post post = postService.getPostById(id);
        if (post != null) {
            return new Status(200, post);
        } else {
            return new Status(404, "No such post");
        }
    }

    /**
     * Update post info
     * PUT /api/v1/posts/:post_id
     *
     * @param id int
     * @param payload Payload
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody
    Status updatePost(@PathVariable("id") int id,
                      @RequestBody Payload payload,
                      @RequestParam String account, String password) {
        if (!userService.authUser(account, password)) {
            return new Status(401, "Not authorized");
        }
        if (!checkOwner(account, postService.getPhotoId(id))) {
            return new Status(403, "Forbidden");
        }
        Post post = postService.getPostById(id);
        post.setContent(payload.getContent());
        if (postService.modPost(post)) {
            return new Status(202, postService.getPostById(id));
        } else {
            return new Status(500, "Server error");
        }
    }

    /**
     * Delete a post
     * DELETE /api/v1/posts/:post_id
     *
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delPost(@PathVariable("id") int id,
                   @RequestParam String account, String password) {
        if (userService.authUser(account, password)) {
            if (checkOwner(account, postService.getPhotoId(id))) {
                if (postService.delPost(id)) {
                    return new Status(204, "Post deleted successfully!");
                } else {
                    return new Status(500, "Server Error");
                }
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }


    /* Comments */

    /**
     * Get comments of post
     * GET /api/v1/posts/:post_id/comments
     *
     * @param postId int
     * @return Status
     */
    @RequestMapping(value = "/{post_id}/comments", method = RequestMethod.GET)
    public @ResponseBody
    Status getComments(@PathVariable("post_id") int postId) {
        List<Comment> comments = commentService.getCommentByPostId(postId);
        if (comments != null) {
            return new Status(200, comments);
        } else {
            return new Status(404, null);
        }
    }

    /**
     * Post a comment
     * POST /api/v1/posts/:post_id/comments
     *
     * @param postId int
     * @param account String
     * @param password String
     * @param payload Payload
     * @return Status
     */
    @RequestMapping(value = "/{post_id}/comments", method = RequestMethod.POST)
    public @ResponseBody
    Status addComment(@PathVariable("post_id") int postId,
                      @RequestParam String account, String password,
                      @RequestBody Payload payload) {
        if (userService.authUser(account, password)) {
            Comment comment = new Comment();
            comment.setContent(payload.getContent());
            comment.setPost(postService.getPostById(postId));
            comment.setUser(userService.getUserByAccount(account));
            if (commentService.addComment(comment)) {
                return new Status(201, "Comment done");
            } else {
                return new Status(500, "Server error");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    /**
     * Delete a comment
     * DELETE /api/v1/posts/:post_id/comments/:comment_id
     *
     * @param postId int
     * @param id int
     * @param account String
     * @param password String
     * @return Status
     */
    @RequestMapping(value = "/{post_id}/comments/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delComment(@PathVariable("post_id") int postId,
                      @PathVariable("id") int id,
                      @RequestParam String account, String password) {
        Comment comment = commentService.getCommentById(id);
        if (comment.getPost().getId() != postId) {
            return new Status(400, "Bad request");
        }
        if (userService.authUser(account, password)) {
            if (account.equals(comment.getUser().getAccount())) {
                if (commentService.delComment(id)) {
                    return new Status(204, "Delete success");
                } else {
                    return new Status(500, "Server error");
                }
            } else {
                return new Status(403, "Forbidden");
            }
        } else {
            return new Status(401, "Not authorized");
        }
    }

    private boolean checkOwner(String account, int photoId) {
        return photoService.checkOwner(userService.getUserByAccount(account).getId(), photoId);
    }

}
