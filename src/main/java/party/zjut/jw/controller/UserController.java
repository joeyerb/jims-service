package party.zjut.jw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import party.zjut.jw.model.Status;
import party.zjut.jw.model.User;
import party.zjut.jw.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    Status getAllUsers() {
        List<User> userList = userService.getUserList();
        if (userList != null) {
            return new Status(200, userList);
        } else {
            return new Status(500, "Server Error");
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public @ResponseBody
    Status addUser(@ModelAttribute User user) {
        try {
            if (userService.addUser(user)) {
                return new Status(201, "Register success");
            } else {
                return new Status(403, "Forbidden");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Status(500, e.toString());
        }
    }

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public @ResponseBody
    Status authUser(@ModelAttribute User user) {
        boolean result = userService.authUser(user.getAccount(), user.getPassword());
        if (result) {
            return new Status(200, "Success");
        } else {
            return new Status(401, "Failure");
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Status getUser(@PathVariable("id") int id) {
        User user;
        try {
            user = userService.getUserById(id);
            if (user != null) {
                return new Status(200, user);
            } else {
                return new Status(404, "No such user");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Status(500, e.toString());
        }
    }

    // Disabled
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    Status delUser(@PathVariable("id") int id) {
        try {
            userService.delUser(id);
            return new Status(204, "User deleted successfully!");
        } catch (Exception e) {
            e.printStackTrace();
            return new Status(500, e.toString());
        }
    }

    /*
    TODO
        getUserPosts
        getUserBookmarks
        getUserFollowings
     */
}
