package party.zjut.jw.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Album;

import java.util.List;

public class AlbumDAO implements IAlbumDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addAlbum(Album album) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(album);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delAlbum(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Album.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    // TODO
    public void modAlbum(Album album) throws Exception {}

    public Album getAlbumById(int id) throws Exception {
        Album album;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            album = session.get(Album.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return album;
    }

    @SuppressWarnings("unchecked")
    public List<Album> findAlbumByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Album> getAlbumList() throws Exception {
        List<Album> albumList;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            albumList = session.createCriteria(Album.class).list();
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return albumList;
    }

}
