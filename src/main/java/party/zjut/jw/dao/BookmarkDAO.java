package party.zjut.jw.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Bookmark;

import java.util.List;

public class BookmarkDAO implements IBookmarkDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addBookmark(Bookmark bookmark) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(bookmark);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delBookmark(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Bookmark.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public Bookmark getBookmarkById(int id) throws Exception {
        Bookmark bookmark;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            bookmark = session.get(Bookmark.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return bookmark;
    }

    @SuppressWarnings("unchecked")
    public List<Bookmark> findBookmarkByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

}
