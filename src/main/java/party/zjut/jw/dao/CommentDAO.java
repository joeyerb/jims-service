package party.zjut.jw.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Comment;

import java.util.List;

public class CommentDAO implements ICommentDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addComment(Comment comment) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(comment);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delComment(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Comment.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    // TODO
    public void modComment(Comment comment) throws Exception {}

    public Comment getCommentById(int id) throws Exception {
        Comment comment;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            comment = session.get(Comment.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return comment;
    }

    @SuppressWarnings("unchecked")
    public List<Comment> findCommentByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

}
