package party.zjut.jw.dao;

import party.zjut.jw.model.Album;

import java.util.List;

public interface IAlbumDAO {
    void addAlbum(Album album) throws Exception;
    void delAlbum(int id) throws Exception;
    void modAlbum(Album album) throws Exception;
    Album getAlbumById(int id) throws Exception;
    List<Album> findAlbumByHql(String hql) throws Exception;
    List<Album> getAlbumList() throws Exception;
}
