package party.zjut.jw.dao;

import party.zjut.jw.model.Bookmark;

import java.util.List;

public interface IBookmarkDAO {
    void addBookmark(Bookmark bookmark) throws Exception;
    void delBookmark(int id) throws Exception;
    Bookmark getBookmarkById(int id) throws Exception;
    List<Bookmark> findBookmarkByHql(String hql) throws Exception;
}
