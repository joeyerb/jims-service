package party.zjut.jw.dao;

import party.zjut.jw.model.Comment;

import java.util.List;

public interface ICommentDAO {
    void addComment(Comment comment) throws Exception;
    void delComment(int id) throws Exception;
    void modComment(Comment comment) throws Exception;
    Comment getCommentById(int id) throws Exception;
    List<Comment> findCommentByHql(String hql) throws Exception;
}
