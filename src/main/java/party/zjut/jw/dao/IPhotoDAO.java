package party.zjut.jw.dao;

import party.zjut.jw.model.Photo;

import java.util.List;

public interface IPhotoDAO {
    void addPhoto(Photo photo) throws Exception;
    void delPhoto(int id) throws Exception;
    void modPhoto(Photo photo) throws Exception;
    Photo getPhotoById(int id) throws Exception;
    List<Photo> findPhotoByHql(String hql) throws Exception;
    List<Photo> getPhotoList() throws Exception;
}
