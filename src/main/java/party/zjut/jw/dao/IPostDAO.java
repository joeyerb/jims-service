package party.zjut.jw.dao;

import party.zjut.jw.model.Post;

import java.util.List;

public interface IPostDAO {
    void addPost(Post post) throws Exception;
    void delPost(int id) throws Exception;
    void modPost(Post post) throws Exception;
    Post getPostById(int id) throws Exception;
    List<Post> findPostByHql(String hql) throws Exception;
    List<Post> getPostList() throws Exception;
}
