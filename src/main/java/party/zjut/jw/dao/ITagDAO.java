package party.zjut.jw.dao;

import party.zjut.jw.model.Tag;

import java.util.List;

public interface ITagDAO {
    void addTag(Tag tag) throws Exception;
    void delTag(int id) throws Exception;
    Tag getTagById(int id) throws Exception;
    List<Tag> findTagByHql(String hql) throws Exception;
    List<Tag> getTagList() throws Exception;
}