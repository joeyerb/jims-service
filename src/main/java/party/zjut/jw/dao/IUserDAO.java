package party.zjut.jw.dao;

import party.zjut.jw.model.User;

import java.util.List;

public interface IUserDAO {
    void addUser(User user) throws Exception;
    void delUser(int id) throws Exception;
    void modUser(User user) throws Exception;
    User getUserById(int id) throws Exception;
    List<User> findUserByHql(String hql) throws Exception;
    List<User> getUserList() throws Exception;
}
