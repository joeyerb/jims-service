package party.zjut.jw.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Photo;

import java.util.List;

public class PhotoDAO implements IPhotoDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addPhoto(Photo photo) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(photo);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delPhoto(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Photo.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void modPhoto(Photo photo) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.update(photo);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public Photo getPhotoById(int id) throws Exception {
        Photo photo;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            photo = session.get(Photo.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return photo;
    }

    @SuppressWarnings("unchecked")
    public List<Photo> findPhotoByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Photo> getPhotoList() throws Exception {
        List<Photo> photoList;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            photoList = session.createCriteria(Photo.class).list();
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return photoList;
    }

}
