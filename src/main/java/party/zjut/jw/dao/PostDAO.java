package party.zjut.jw.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Post;

import java.util.List;

public class PostDAO implements IPostDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addPost(Post post) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(post);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delPost(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Post.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void modPost(Post post) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.update(post);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public Post getPostById(int id) throws Exception {
        Post post;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            post = session.get(Post.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return post;
    }

    @SuppressWarnings("unchecked")
    public List<Post> findPostByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Post> getPostList() throws Exception {
        List<Post> postList;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            postList = session.createCriteria(Post.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return postList;
    }

}
