package party.zjut.jw.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.Tag;

import java.util.List;

public class TagDAO implements ITagDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addTag(Tag tag) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(tag);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delTag(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(Tag.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public Tag getTagById(int id) throws Exception {
        Tag tag;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            tag = session.get(Tag.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return tag;
    }

    @SuppressWarnings("unchecked")
    public List<Tag> findTagByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Tag> getTagList() throws Exception {
        List<Tag> tagList;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            tagList = session.createCriteria(Tag.class)
                    .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return tagList;
    }
}
