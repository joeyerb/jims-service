package party.zjut.jw.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.model.User;

public class UserDAO implements IUserDAO {

    @Autowired
    SessionFactory sessionFactory;

    Transaction tran = null;
    Session session = null;

    public void addUser(User user) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.save(user);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void delUser(int id) throws Exception {
        try {
            session = sessionFactory.openSession();
            Object obj = session.load(User.class, id);
            tran = session.beginTransaction();
            session.delete(obj);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public void modUser(User user) throws Exception {
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            session.update(user);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
    }

    public User getUserById(int id) throws Exception {
        User user;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            user = session.get(User.class, id);
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> findUserByHql(String hql) throws Exception {
        try {
            session = sessionFactory.openSession();
            return session.createQuery(hql).list();
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> getUserList() throws Exception {
        List<User> userList;
        try {
            session = sessionFactory.openSession();
            tran = session.beginTransaction();
            userList = session.createCriteria(User.class).list();
            tran.commit();
        } catch (RuntimeException re) {
            if (tran != null) tran.rollback();
            throw re;
        } finally {
            session.close();
        }
        return userList;
    }

}
