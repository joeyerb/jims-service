package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.AlbumDAO;
import party.zjut.jw.model.Album;

import java.util.List;

public class AlbumService implements IAlbumService {

    @Autowired
    AlbumDAO albumDAO;

    public boolean addAlbum(Album album) {
        try {
            albumDAO.addAlbum(album);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delAlbum(int id) {
        try {
            albumDAO.delAlbum(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean modAlbum(Album album) {
        try {
            albumDAO.modAlbum(album);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Album getAlbumById(int id) {
        try {
            return albumDAO.getAlbumById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Album getAlbumByUserId(Integer userId) {
        String hql = "from Album as album where user_id='" + userId.toString() + "'";
        try {
            List<Album> albumList = albumDAO.findAlbumByHql(hql);
            if (albumList.size() == 1) { return albumList.get(0); }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Album> getAlbumList() {
        try {
            return albumDAO.getAlbumList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean checkOwner(Integer userId, Integer albumId) {
        String hql = "from Album as album where user_id='" +
                userId.toString() + "' AND id='" + albumId.toString() + "'";
        try {
            List<Album> albumList = albumDAO.findAlbumByHql(hql);
            if (albumList.size() == 1) { return true; }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
