package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.BookmarkDAO;
import party.zjut.jw.model.Bookmark;
import party.zjut.jw.model.Photo;
import party.zjut.jw.model.Post;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class BookmarkService implements IBookmarkService {

    @Autowired
    BookmarkDAO bookmarkDAO;
    @Autowired
    PostService postService;
    @Autowired
    UserService userService;

    public boolean addBookmark(String account, int postId, int rating) {
        Bookmark bk = new Bookmark();
        bk.setPost(postService.getPostById(postId));
        bk.setUserId(userService.getUserByAccount(account).getId());
        bk.setRating(rating);
        return addBookmark(bk);
    }

    public boolean addBookmark(Bookmark bookmark) {
        try {
            Date date = new Date();
            bookmark.setCreatedTime(new Timestamp(date.getTime()));
            bookmarkDAO.addBookmark(bookmark);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delBookmark(int id) {
        try {
            Bookmark bk = getBookmarkById(id);
            Post post = bk.getPost();
            if (post.getBookmarks().remove(bk)) {
                postService.modPost(post);
                //bookmarkDAO.delBookmark(id);
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Bookmark getBookmarkById(int id) {
        try {
            return bookmarkDAO.getBookmarkById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Bookmark> getBookmarksByUserId(Integer userId) {
        String hql = "from Bookmark as bookmark where user_id='" + userId.toString() + "'";
        try {
            return addUrl(bookmarkDAO.findBookmarkByHql(hql));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Bookmark> getBookmarksByPostId(Integer postId) {
        String hql = "from Bookmark as bookmark where post_id='" + postId.toString() + "'";
        try {
            return addUrl(bookmarkDAO.findBookmarkByHql(hql));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private List<Bookmark> addUrl(List<Bookmark> bks) {
        String url_prefix = "/images/";
        for (Bookmark bk : bks) {
            if (bk != null) {
                Photo photo = bk.getPost().getPhoto();
                photo.setUrl(url_prefix + photo.getTitle() + ".jpg");
            }
        }
        return bks;
    }
}
