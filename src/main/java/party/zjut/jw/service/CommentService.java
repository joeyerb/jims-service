package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.CommentDAO;
import party.zjut.jw.model.Comment;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class CommentService implements ICommentService {

    @Autowired
    CommentDAO commentDAO;

    public boolean addComment(Comment comment) {
        try {
            Date date = new Date();
            comment.setPostTime(new Timestamp(date.getTime()));
            commentDAO.addComment(comment);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // Admin
    public boolean delComment(int id) {
        try {
            commentDAO.delComment(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean modComment(Comment comment) {
        try {
            commentDAO.modComment(comment);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Comment getCommentById(int id) {
        try {
            return commentDAO.getCommentById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Comment> getCommentByPostId(Integer postId) {
        String hql = "from Comment as comment where post_id='" +
                postId.toString() + "' order by comment.id";
        try {
            List<Comment> commentList = commentDAO.findCommentByHql(hql);
            if (commentList.size() >= 0) { return commentList; }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
