package party.zjut.jw.service;

import party.zjut.jw.model.Album;

import java.util.List;

public interface IAlbumService {
    boolean addAlbum(Album album);
    boolean delAlbum(int id);
    boolean modAlbum(Album album);
    Album getAlbumById(int id);
    Album getAlbumByUserId(Integer userId);
    List<Album> getAlbumList();
}
