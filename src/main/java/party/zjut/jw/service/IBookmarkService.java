package party.zjut.jw.service;

import party.zjut.jw.model.Bookmark;

import java.util.List;

public interface IBookmarkService {
    boolean addBookmark(Bookmark bookmark);
    boolean delBookmark(int id);
    Bookmark getBookmarkById(int id);
    List<Bookmark> getBookmarksByUserId(Integer userId);
    List<Bookmark> getBookmarksByPostId(Integer postId);
}
