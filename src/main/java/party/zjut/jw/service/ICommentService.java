package party.zjut.jw.service;

import party.zjut.jw.model.Comment;

import java.util.List;

public interface ICommentService {
    boolean addComment(Comment comment);
    boolean delComment(int id);
    boolean modComment(Comment comment);
    Comment getCommentById(int id);
    List<Comment> getCommentByPostId(Integer postId);
}
