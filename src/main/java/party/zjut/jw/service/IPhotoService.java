package party.zjut.jw.service;

import party.zjut.jw.model.Photo;

import java.util.List;

public interface IPhotoService {
    boolean addPhoto(Photo photo);
    boolean delPhoto(int id);
    boolean modPhoto(Photo photo);
    Photo getPhotoById(int id);
    List<Photo> getPhotosByUserId(Integer userId);
    List<Photo> getPhotoList();
}
