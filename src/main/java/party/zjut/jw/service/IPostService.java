package party.zjut.jw.service;

import party.zjut.jw.model.Payload;
import party.zjut.jw.model.Post;
import party.zjut.jw.model.Post;

import java.util.List;

public interface IPostService {
    boolean addPost(Payload payload);
    boolean delPost(int id);
    boolean modPost(Post post);
    Post getPostById(int id);
    List<Post> getPostsByUserId(Integer userId);
    List<Post> getPostList();
}
