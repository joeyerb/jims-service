package party.zjut.jw.service;

import party.zjut.jw.model.Tag;

import java.util.List;

public interface ITagService {
    boolean addTag(Tag tag);
    boolean delTag(Tag tag);
    Tag getTagById(int id);
    List<Tag> getTagByPhotoId(Integer postId);
    List<Tag> getAllTags();
}
