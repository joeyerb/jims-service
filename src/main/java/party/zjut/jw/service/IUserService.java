package party.zjut.jw.service;

import party.zjut.jw.model.User;

import java.util.List;

public interface IUserService {
    boolean addUser(User user);
    boolean delUser(int id);
    boolean modUser(User user);
    User getUserById(int id);
    List<User> getUserList();
    boolean authUser(String account, String password);
}
