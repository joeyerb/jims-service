package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import party.zjut.jw.dao.PhotoDAO;
import party.zjut.jw.model.Photo;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class PhotoService implements IPhotoService {

    @Autowired
    PhotoDAO photoDAO;

    public boolean addPhoto(Photo photo) {
        Date date = new Date();
        photo.setUploadedTime(new Timestamp(date.getTime()));
        if (photo.getCreatedTime() == null) {
            photo.setCreatedTime(photo.getUploadedTime());
        }
        try {
            photoDAO.addPhoto(photo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delPhoto(int id) {
        try {
            photoDAO.delPhoto(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean modPhoto(Photo photo) {
        try {
            photoDAO.modPhoto(photo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Photo getPhotoById(int id) {
        try {
            return addUrl(photoDAO.getPhotoById(id));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Photo> getPhotosByUserId(Integer userId) {
        String hql = "from Photo as photo where user_id='" + userId.toString() + "'";
        try {
            List<Photo> photos = photoDAO.findPhotoByHql(hql);
            for (Photo photo : photos) {
                addUrl(photo);
            }
            return photos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Photo> getPhotoList() {
        try {
            List<Photo> photos = photoDAO.getPhotoList();
            for (Photo photo : photos) {
                addUrl(photo);
            }
            return photos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean checkOwner(Integer userId, Integer photoId) {
        String hql = "from Photo as photo where user_id='" +
                userId.toString() + "' AND id='" + photoId.toString() + "'";
        try {
            List<Photo> photoList = photoDAO.findPhotoByHql(hql);
            if (photoList.size() == 1) { return true; }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean savePhoto(MultipartFile file, String name) {
        try {
            byte[] bytes = file.getBytes();

            File dir = new File("/var/lib/tomcat8/webapps/images");
            if (!dir.exists()) {
                dir.mkdirs();
            }

            File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Photo addUrl(Photo photo) {
        if (photo != null) {
            String url_prefix = "/images/";
            photo.setUrl(url_prefix + photo.getTitle() + ".jpg");
        }
        return photo;
    }
}
