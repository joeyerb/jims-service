package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.PostDAO;
import party.zjut.jw.model.Payload;
import party.zjut.jw.model.Photo;
import party.zjut.jw.model.Post;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class PostService implements IPostService {

    @Autowired
    PostDAO postDAO;
    @Autowired
    PhotoService photoService;

    public boolean addPost(Payload payload) {
        try {
            Post post = new Post();
            post.setPostType(payload.getPostType());
            post.setContent(payload.getContent());
            post.setPublicity(payload.getPublicity());
            post.setPhoto(photoService
                    .getPhotoById(payload.getPhotoId()));
            Date date = new Date();
            post.setPostTime(new Timestamp(date.getTime()));
            postDAO.addPost(post);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delPost(int id) {
        try {
            postDAO.delPost(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean modPost(Post post) {
        try {
            postDAO.modPost(post);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Post getPostById(int id) {
        try {
            Post post = postDAO.getPostById(id);
            return fillPost(post);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // TODO
    public List<Post> getPostsByUserId(Integer userId) {
        String hql = "from Post as post where user_id='" + userId.toString() + "'";
        try {
            List<Post> postList = postDAO.findPostByHql(hql);
            if (postList.size() > 0) {
                for (Post post : postList) {
                    fillPost(post);
                }
            }
            return postList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Post> getPostList() {
        try {
            List<Post> postList = postDAO.getPostList();
            if (postList.size() > 0) {
                for (Post post : postList) {
                    fillPost(post);
                }
            }
            return postList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getPhotoId(Integer postId) {
        String hql = "from Post as post where id='" +
                postId.toString() + "'";
        try {
            List<Post> postList = postDAO.findPostByHql(hql);
            if (postList.size() == 1) {
                return postList.get(0).getPhoto().getId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private Post fillPost(Post post) {
        if (post != null) {
            String url_prefix = "/images/";
            Photo photo = post.getPhoto();
            photo.setUrl(url_prefix + photo.getTitle() + ".jpg");
            //for (Bookmark bk : post.getBookmarks()) {
            //    bk.setUserId(bk.getUser().getId());
            //}
        }
        return post;
    }
}
