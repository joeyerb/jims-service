package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.TagDAO;
import party.zjut.jw.model.Photo;
import party.zjut.jw.model.Status;
import party.zjut.jw.model.Tag;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TagService implements ITagService {

    @Autowired
    TagDAO tagDAO;
    @Autowired
    PhotoService photoService;

    public boolean addTag(Tag tag) {
        try {
            tagDAO.addTag(tag);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // Admin
    public boolean delTag(Tag tag) {
        try {
            tag.setPhotos(new HashSet<Photo>());
            tagDAO.delTag(tag.getId());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Tag getTagById(int id) {
        try {
            return tagDAO.getTagById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Tag getTagByName(String name) {
        String hql = "from Tag where name = '" + name + "'";
        try {
            List<Tag> tagList = tagDAO.findTagByHql(hql);
            if (tagList.size() > 0) {
                return tagList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Tag> getAllTags() {
        try {
            return tagDAO.getTagList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Tag> getTagByPhotoId(Integer photoId) {
        String hql = "from photo_tag where photo_id='" + photoId.toString() + "'";
        try {
            List<Tag> tagList = tagDAO.findTagByHql(hql);
            if (tagList.size() >= 0) { return tagList; }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // TODO count
    public boolean addPhotoTag(Photo photo, Tag tag) {
        Tag itag = getTagByName(tag.getName());
        if (itag == null) {
            addTag(tag);
            itag = getTagByName(tag.getName());
        }
        Set<Tag> tags = photo.getTags();
        if (!tags.contains(itag)) {
            tags.add(itag);
            photo.setTags(tags);
            photoService.modPhoto(photo);
        }
        return true;
    }

    public boolean delPhotoTag(Photo photo, Tag tag) {
        Set<Tag> tags = photo.getTags();
        if (tags.contains(tag)) {
            tags.remove(tag);
            photo.setTags(tags);
            photoService.modPhoto(photo);
        }
        return true;
    }
}
