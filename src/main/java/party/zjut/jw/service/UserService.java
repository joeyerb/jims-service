package party.zjut.jw.service;

import org.springframework.beans.factory.annotation.Autowired;
import party.zjut.jw.dao.UserDAO;
import party.zjut.jw.model.User;

import java.util.List;

public class UserService implements IUserService {

    @Autowired
    UserDAO userDAO;

    public boolean addUser(User user) {
        try {
            userDAO.addUser(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delUser(int id) {
        try {
            userDAO.delUser(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean modUser(User user) {
        try {
            userDAO.modUser(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public User getUserById(int id) {
        User user;
        try {
            user = userDAO.getUserById(id);
            user.setPassword(null);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getUserByAccount(String account) {
        String hql = "from User as user where account='" + account + "'";
        try {
            List<User> userList = userDAO.findUserByHql(hql);
            if (userList.size() == 1) {
                User user = userList.get(0);
                user.setPassword(null);
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getUserList() {
        try {
            return userDAO.getUserList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean authUser(String account, String password) {
        String hql = "from User as user where account='" +
                account + "' and password='" + password + "'";
        try {
            List<User> userList = userDAO.findUserByHql(hql);
            return userList.size() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
